# This is my conf file. There are many like it, but this one is mine.
class people::dclas {

  # Symlink to fix Atom install, see
  # https://github.com/caskroom/homebrew-cask/issues/4716
  file { '/usr/local':
    ensure => link,
    target => '/opt/boxen',
    force => true
  }

  # Configure Atom
  include atom
  atom::package { 'language-puppet': }

  # OSX Tweaks
  include osx::global::expand_save_dialog
  include osx::global::disable_remote_control_ir_receiver
  include osx::dock::autohide
  include osx::dock::clear_dock
  include osx::finder::enable_quicklook_text_selection
  include osx::safari::enable_developer_mode
  include osx::no_network_dsstores
  include osx::finder::unhide_library

  class { 'osx::global::natural_mouse_scrolling':
    enabled => false
  }
  class { 'osx::dock::position':
    position => 'right'
  }
  class { 'osx::mouse::swipe_between_pages':
    enabled => true
  }

  # Create ssh key
  exec {'Generate SSH key for Github':
    command => "ssh-keygen -t rsa -b 2048 -C 'dave@macbook' -f /Users/dave/.ssh/id_rsa -N ''",
    unless  => "test -e /Users/dave/.ssh/id_rsa"
  }

  # Node modules
  npm_module { 'yo':module => 'yo', node_version => '*' }
  npm_module { 'bower':module => 'bower', node_version => '*'}
  npm_module { 'grunt-cli':module => 'grunt-cli', node_version => '*' }
  npm_module { 'generator-jhipster':module => 'generator-jhipster', node_version => '*' }
  npm_module { 'compass':module => 'compass', node_version => '*' }
  npm_module { 'jhipster-uml':module => 'jhipster-uml', node_version => '*' }
  npm_module { 'mean-cli':module => 'mean-cli', node_version => '*' }
  npm_module { 'gulp':module => 'gulp', node_version => '*' }
  npm_module { 'browser-sync':module => 'browser-sync', node_version => '*' }
  npm_module { 'cordova':module => 'cordova', node_version => '*' }
  npm_module { 'ionic':module => 'ionic', node_version => '*' }
  npm_module { 'ios-sim':module => 'ios-sim', node_version => '*' }

  #App Store applications
  #https://itunes.apple.com/us/app/skitch-snap.-mark-up.-share./id425955336?mt=12
  #appstore::app { 'Skitch': source => 'skitch-snap.-mark-up.-share./id425955336' }
  #https://itunes.apple.com/us/app/microsoft-onenote/id784801555?mt=12
  #appstore::app { 'Microsoft OneNote': source => 'microsoft-onenote/id784801555' }
  #https://itunes.apple.com/us/app/keynote/id409183694?mt=12
  #appstore::app { 'Keynote': source => 'keynote/id409183694' }
  #https://itunes.apple.com/us/app/numbers/id409203825?mt=12
  #appstore::app { 'Numbers': source => 'numbers/id409203825' }
  #https://itunes.apple.com/us/app/pages/id409201541?mt=12
  #appstore::app { 'Pages': source => 'pages/id409201541' }
  #https://itunes.apple.com/us/app/pixelmator/id407963104?mt=12
  #appstore::app { 'Pixelmator': source => 'pixelmator/id407963104' }



  # Ruby gems
  ruby_gem { 'compass':gem => 'compass', version => '1.0.3', ruby_version => '2.1.2' }

  # Development software packages
  package { 'java': provider => 'brewcask'}
  package { 'sts': provider => 'brewcask' }
  package { 'postgres': provider => 'brewcask'}
  package { 'pgadmin3': provider => 'brewcask'}
  package { 'iterm2': provider => 'brewcask'}
  package { '1password': provider => 'brewcask'}
  package { 'quicksilver': provider => 'brewcask'}
  package { 'dropbox': provider => 'brewcask'}
  package { 'google-chrome': provider => 'brewcask'}
  package { 'maven': provider => 'homebrew'}
  package { 'sourcetree': provider => 'brewcask'}
  package { 'staruml': provider => 'brewcask'}
  package { 'mongodb': provider => 'brewcask'}
  package { 'vmware-horizon-client': provider => 'brewcask'}
  package { 'p4merge': provider => 'brewcask'}
  package { 'firefox': provider => 'brewcask'}
  package { 'evernote': provider => 'brewcask' }
  package { 'chromium': provider => 'brewcask' }
  package { 'python': provider => 'homebrew'}
  package { 'pyenv': provider => 'homebrew'}
  package { 'pyenv-virtualenv': provider => 'homebrew'}
  package { 'zsh': provider => 'homebrew'}
  package { 'zsh-completions': provider => 'homebrew'}

}
